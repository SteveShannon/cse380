#!/bin/bash

# This program takes 3 command line input arguments (see below), numerically integrates integral(cos(x)dx) from xmin to xmax


#
# usage information that will be printed if "-h" is the first command line input
#
usage() {
echo ""
echo "Invoke the script in the following manner:"
echo ""
echo "./integrate_cosine.sh xmin xmax NP"
echo ""
echo "where:   xmin: lower integration bound"
echo "         xmax: upper integration bound"
echo "         NP: number of sample points for numerical integration"
echo ""
}

#
# testing input (should add more here)
#
if [ $1 = "-h" 2> /dev/null ]; then
	usage
	exit 1
elif [ $# -ne 3 ]; then
	usage
	exit 1
fi


xmin=$1		# Left integration bound
xmax=$2		# Right integration bound
NP=$3		# Number of sample points for numerical integration

scale=10	# Number of decimal places used throughout floating point calculations

#
# perform numerical integration
#
h=`echo "scale=$scale; ($xmax - $xmin)/$NP" | bc -l`
integral=0
x=`echo "scale=$scale; $xmin + 0.5*$h" | bc -l`
for i in `seq 1 $NP`; do
	integral=`echo "scale=$scale; $integral + c($x)" | bc -l`
	x=`echo "scale=$scale; $x + $h" | bc -l`
done
integral=`echo "scale=$scale; $integral*$h" | bc -l`

#
# caluclate exact solution and calculate error (in absolute value)
#
integral_exact=`echo "scale=$scale; s($xmax) - s($xmin)" | bc -l`
error=`echo "scale=$scale; $integral - $integral_exact" | bc -l`
if [ `echo "$error < 0" | bc -l` -eq 1 ]; then
	error=`echo "scale=$scale; 0 - $error" | bc -l`
fi


echo ""
echo "Result of numerical integration: $integral"
echo "Exact value of the integral:     $integral_exact"
echo "Error of numerical integration:  $error"
echo ""
