#!/bin/bash

# This program invokes integrate_cosine.sh multiple times with different numbers of integration points and produces the following plots:
	# wallclock runtime vs. number of integration points
	# error of numerical integration vs. number of integration points


#
# set integration bounds and and number of integration points (for midpoint rule) to integrate cos(x)
#
xmin=0
xmax=5
NP=(10 50 100 200 400 1000)


#
# loop through integration points and save absolute error to one array, run time to another array
#
j=0
for i in ${NP[@]}; do
	> temp.dat
	`/usr/bin/time -f "%e" bash ./integrate_cosine.sh $xmin $xmax $i >> temp.dat 2>&1`
	E[$j]=`cat temp.dat | grep "Error" | cut -f 2 -d :`
	T[$j]=`cat temp.dat | tail -n 1`
#	E[$j]=`./integrate_cosine.sh $xmin $xmax $i`
	j=$(($j+1))
done
rm temp.dat


#
# organize data in columns (col 1: # integration points, col 2: absolute error, col 3: time to run) 
#
> results.dat
for i in `seq 1 $j`; do
	echo "${NP[$i-1]} ${E[$i-1]} ${T[$i-1]}" >> results.dat
done


#
# generate plots of run time vs. NP, absolute error vs. NP
#
gnuplot << HERE
set terminal png size 600,750
set output 'results.png'
set multiplot layout 2,1
set logscale xy
set title "Error vs. number of integration points of cos(x) from $xmin to $xmax"
set xlabel "number of integration points"
set ylabel "absolute error"
plot 'results.dat' using 1:2 w linespoints notitle
unset logscale xy
set title "Wallclock runtime vs. number of integration points of cos(x) from $xmin to $xmax"
set xlabel "number of integration points"
set ylabel "wallclock runtime in seconds"
plot 'results.dat' using 1:3 w linespoints notitle
unset multiplot
HERE

#
# alternate method
#
#gnuplot << HERE
#set size 1,1
#set terminal png size 600,750
#set output 'results.png'
#set multiplot
#set size 1,0.5
#set origin 0,0.5
#set xlabel "number of integration points"
#set ylabel "absolute error"
#set title "Error vs. number of integration points of cos(x) from $xmin to $xmax"
#set logscale xy
#plot 'results.dat' using 1:2 w linespoints notitle
#set origin 0,0
#set xlabel "number of integration points"
#set ylabel "wallclock runtime in seconds"
#set title "Wallclock runtime vs. number of integration points of cos(x) from $xmin to $xmax"
#unset logscale xy
#plot 'results.dat' using 1:3 w linespoints notitle
#unset multiplot
#set output
#HERE


gnome-open results.png
