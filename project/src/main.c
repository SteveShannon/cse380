#include <stdio.h>
#include <stdlib.h>		// for dynamic memory allocation
#include <math.h>
#include <stdarg.h>
#include <string.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <grvy.h>


void read_input();
void eq1_meth1();
void eq1_meth2();
void eq1_solve();
void eq1_verify();
void eq1_conv();
void eq2_meth1();
void eq2_meth2();
void eq2_meth3();
void eq2_solve();
void eq2_conv();
void maxabs();
void test_malloc();
void write_output();

int g_argc;
char **g_argv;

//
// main driver function
//
int main(int argc, char **argv)
{
	grvy_timer_begin(__func__);

	g_argc = argc;
	g_argv = argv;

	// reading input
	int eq_num, sol_method, conv, ver_mode, verb, n;
	read_input(&eq_num, &sol_method, &conv, &ver_mode, &verb, &n);

	double *x = NULL;
	double *y = NULL;
	double *y_exact = NULL;
	double *e = NULL;
	int *m = NULL;
	double *e_Linf = NULL;
	int a = 0;
	int b = 0;

	int i;
	if(eq_num == 1)		// equation 1
	{
		if(conv == 1)	// equation 1 convergence analysis
		{
			// define limits of convergence analysis 2^a and 2^b
			if(sol_method == 1)
			{
				a = 3;
				b = 20;
			}
			else
			{
				a = 5;
				b = 20;
			}


			// generate results
			eq1_conv(&x, &y, &y_exact, &e, &m, &e_Linf,
					a, b, sol_method, &n);

		}
		else			// equation 1 no convergence analysis
		{
			// allocate memory
			x = malloc((n+1)*sizeof(double));
			test_malloc(x);
			y = malloc((n+1)*sizeof(double));
			test_malloc(y);

			// generate results
			eq1_solve(x,y,n,sol_method);

			if(ver_mode == 1)	// verification mode
			{
				// allocate more memory
				y_exact = malloc((n+1)*sizeof(double));
				test_malloc(y_exact);
				e = malloc((n+1)*sizeof(double));
				test_malloc(e);
				e_Linf = malloc(sizeof(double));
				test_malloc(e_Linf);

				// generate more results
				eq1_verify(x, y, y_exact, e, e_Linf, n);
			}
		}
	}
	else				// equation 2
	{
		if(conv == 1)	// equation 2 convergence analysis
		{
			// define limits of convergence analysis 2^a and 2^b
			if(sol_method == 1){
				a = 6;
				b = 16;
			}
			else if(sol_method == 2){
				a = 6;
				b = 15;
			}
			else
			{
				a = 5;
				b = 10;
			}

			// generate results
			eq2_conv(&x, &y, &y_exact, &m, &e_Linf, a, b, sol_method, &n);

		}
		else			// equation 2 no convergence analysis
		{
			// allocate memory
			x = malloc((n+1)*sizeof(double));
			test_malloc(x);
			y = malloc(3*(n+1)*sizeof(double));
			test_malloc(y);

			// generate results
			eq2_solve(x,y,n,sol_method);
		}
	}
	
	// write data to file(s)
	write_output(x, y, y_exact, e, m, e_Linf, eq_num,
		   sol_method, conv, ver_mode, n, a, b, verb);

	// free memory
	if(x != NULL)
		free(x); x = NULL;
	if(y != NULL)
		free(y); y = NULL;
	if(y_exact != NULL)
		free(y_exact); y_exact = NULL;
	if(e != NULL)
		free(e); e = NULL;
	if(m != NULL)
		free(m); m = NULL;
	if(e_Linf != NULL)
		free(e_Linf); e_Linf = NULL;

	grvy_timer_end(__func__);

	grvy_timer_finalize();

	/* Print performance summary to stdout */

	grvy_timer_summarize();



	return(0); 
}


//
// function reads input from input.dat
//
void read_input(int *eq_num, int *sol_method, int *conv,
		int *ver_mode, int *verb,int *n)
{
	grvy_timer_begin(__func__);

	// we will read in each line of the input file and save it temporarily to this array
	char filename[50];
	if(g_argc>1)
		strcpy(filename,g_argv[1]);
	else
		strcpy(filename,"./input.dat");

	int i;
	FILE *ifp = fopen(filename,"r");
	char line[500];

	// reading equation number (eq_num) and checking if input is valid
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",eq_num);
	if(*eq_num != 1 && *eq_num != 2)
	{			
		printf("\nError: equation to solve must be:\n"
				"          \"1\" (Simple ODE)\n"
				"          \"2\" (Charged particle motion in a vertical electric field)\n\n"
				"Modify line 1 of input.dat\n\n");
		exit(1);
	}

	// reading solution method (sol_method) and checking if input is valid
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",sol_method);
	if(*eq_num == 1)
	{
		if(*sol_method != 1 && *sol_method != 2)
		{
			printf("\nError: if solving equation 1, solution method must be:\n"
					"          \"1\" (explicit Euler)\n"
					"          \"2\" (tpl)\n"
					"       if solving equation 2, solution method must be:\n"
					"          \"1\" (RK4)\n"
					"          \"2\" (asdfasdf)\n"
					"          \"3\" (asdfasdf)\n\n"
					"Modify line 2 of input.dat\n\n");
			exit(2);
		}
	}
	else
	{
		if(*sol_method != 1 && *sol_method != 2 && *sol_method !=3)
		{
			printf("\nError: if solving equation 1, solution method must be:\n"
					"          \"1\" (explicit Euler)\n"
					"          \"2\" (tpl)\n"
					"       if solving equation 2, solution method must be:\n"
					"          \"1\" (RK4)\n"
					"          \"2\" (asdfasdf)\n"
					"          \"3\" (asdfasdf)\n\n"
					"Modify line 2 of input.dat\n\n");
			exit(2);
		}
	}

	// reading convergence analysis flag (conv) and checking if input is valid
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",conv);	
	if(*conv != 0 && *conv != 1)
	{
		printf("\nError: convergence analysis flag must be:\n"
				"          \"0\" (not active)\n"
				"          \"1\" (active)\n\n"
				"Modify line 3 of input.dat\n\n");
		exit(3);
	}

	// reading verification mode flag (ver_mode) and checking if input is valid
	// Verification mode is only an option for eq1.
	// Verification mode information is calculated as part of convergence analysis,
	// so verification mode flag is ignored when convergence analysis flag is 1.
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",ver_mode);
	if(*eq_num == 1)
	{
		if(*conv == 0)
		{
			if(*ver_mode != 0 && *ver_mode != 1)
			{
				printf("\nError: verification mode must be:\n"
						"          \"0\" (not active)\n"
						"          \"1\" (active)\n\n"
						"Modify line 4 of input.dat\n\n");
				exit(4);
			}
		}
		else
		{
			*ver_mode = 1; // (for file naming and output for eq1)
		}
	}

	// reading verbosity flag
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",verb);
	if(*verb != 0 && *verb != 1)
	{
		printf("\nError: output file verbosity must be:\n"
				"          \"0\" (not \"verbose\")\n"
				"          \"1\" (\"verbose\", i.e., complete output)\n\n"
				"Modify line 5 of input.dat\n\n");
		exit(5);
	}
	// reading number of steps (n)
	fgets(line,sizeof(line),ifp);
	sscanf(line,"%d",n);

	// close input file
	fclose(ifp);

	grvy_timer_end(__func__);
}


////
//// function calculates y(x), x in [-1,3] using forward Euler
//// dy/dx = -2xx; y(-1) = 1/e
////
//void eq1_meth1(double *x, double *y, int n)
//{
//	double h,a,sum;
//	int i;
//	h = 4.0/n;		// delta t (3 - -1 = 4)
//	x[0] = -1;		// value of x at left boundary
//	y[0] = 1/M_E;	// value of y at left boundary
//	a = 2*h*h;		// we will subtract this from "sum" on each time step
//	sum = 1 + 2*h + a;
//	for (i=1 ; i<n+1 ; i++)
//	{
//		x[i] = x[i-1] + h;
//		sum = sum - a;
//		y[i] = sum*y[i-1];
//	}
//}


//
// function calculates y(x), x in [-1,3] using backward Euler
// dy/dx = -2xx; y(-1) = 1/e
//
void eq1_meth1(double *x, double *y, int n)
{
	grvy_timer_begin(__func__);

	double h,a,sum;
	int i;
	h = 4.0/n;		// delta t (3 - -1 = 4)
	x[0] = -1;		// value of x at left boundary
	y[0] = 1/M_E;	// value of y at left boundary
	a = 2*h*h;		// we will add this from "sum" on each time step
	sum = 1 - 2*h;
	for (i=1 ; i<n+1 ; i++)
	{
		x[i] = x[i-1] + h;
		sum = sum + a;
		y[i] = (1.0/sum)*y[i-1];
	}

	grvy_timer_end(__func__);
}


//
// function calculates y(x), x in [-1,3] using GSL's implicit Euler
// dy/dx = -2xx; y(-1) = 1/e
//
void eq1_meth2(double *x_asdf, double *y_asdf, int n)
{
	grvy_timer_begin(__func__);

	// fuction that will be input for gsl_odeiv2_system
	int func(double x, const double y[], double f[], void *params)
	{
		f[0] = -2*x*y[0];
		return GSL_SUCCESS;
	}

	// jacobian that will be input for gsl_odeiv2_system
	int jac(double x, const double y[], double *dfdy, double dfdt[], void *params)
	{
		dfdy[0] = -2*x;
		dfdt[0] = -2*y[0];
		return GSL_SUCCESS;
	}

	// struct gsl_odeiv2_system has 4 inputs:
	//		int (* function) (double t, const double y[], double dydt[], void * params)
	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
	//		size_t dimension
	//		void * params
	gsl_odeiv2_system sys = {func, jac, 1, 0};

	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rk1imp;

	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
	//		const gsl_odeiv2_system * sys
	//		const gsl_odeiv2_step_type * 
	//		const double hstart - seems to have no effect when using
	//		                      gsl_odeiv2_driver_apply_fixed_step (below)
	//		const double epsabs
	//		const double epsrel
	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&sys, type, 4.0/n, INFINITY, INFINITY);

	// this is xL...stepping below ensures we stop at xR
	double x = -1.0;	

	// initial (boundary) condition
	double y[1] = { 1/M_E };

	// saving values to input pointers
	x_asdf[0] = x;
	y_asdf[0] = y[0];

	// stepping through and saving solution at every time step
	int i,s;
	for (i = 1; i <= n; i++)
	{
		// function gsl_odeiv2_driver_apply_fixed_step has 5 inputs:
		//		gsl_odeiv2_driver * d
		//		double * t
		//		const double h
		//		const unsigned long int n
		//		double y[]
		s = gsl_odeiv2_driver_apply_fixed_step(d, &x, 4.0/n, 1, y);

		if (s != GSL_SUCCESS)
		{
			printf ("error: driver returned %d\n", s);
			break;
		}

		// saving values to input ppointers
		x_asdf[i] = x;
		y_asdf[i] = y[0];
	}

	// free memory
	gsl_odeiv2_driver_free(d);

	grvy_timer_end(__func__);
}


////
//// function calculates y(x), x in [-1,3] using GSL's implicit Euler
//// dy/dx = -2xx; y(-1) = 1/e
////
//void eq1_meth2(double *x_asdf, double *y_asdf, int n)
//{
//	double h = 4.0/n;
//
//	// fuction that will be input for gsl_odeiv2_system
//	int func(double x, const double y[], double f[], void *params)
//	{
//		f[0] = -2*x*y[0];
//		return GSL_SUCCESS;
//	}
//
//	// jacobian that will be input for gsl_odeiv2_system
//	int jac(double x, const double y[], double *dfdy, double dfdt[], void *params)
//	{
//		dfdy[0] = -2*x;
//		dfdt[0] = -2*y[0];
//		return GSL_SUCCESS;
//	}
//	
//	// struct gsl_odeiv2_system has 4 inputs:
//	//		int (* function) (double t, const double y[], double dydt[], void * params)
//	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
//	//		size_t dimension
//	//		void * params
//	gsl_odeiv2_system sys = {func, jac, 1, 0};
//
//	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rk1imp;
//
//	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
//	//		const gsl_odeiv2_system * sys
//	//		const gsl_odeiv2_step_type * T
//	//		const double hstart
//	//		const double epsabs
//	//		const double epsrel
//	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&sys, type, h, INFINITY, INFINITY);
//
//	// setting hmin and hmax (to be the same...controlling step size)
//	gsl_odeiv2_driver_set_hmin(d,h);
//	gsl_odeiv2_driver_set_hmax(d,h);
//
//	// this is xL...stepping below ensures we stop at xR
//	double x = -1.0;	
//
//	// initial (boundary) condition
//	double y[1] = { 1/M_E };
//
//	// saving values to input pointers
//	x_asdf[0] = x;
//	y_asdf[0] = y[0];
//
//	// stepping through and saving solution at every time step
//	int i,s;
//	for (i = 1; i <= n; i++)
//	m
//		// function gsl_odeiv2_driver_apply has 4 inputs:
//		//		gsl_odeiv2_driver * d
//		//		double * t
//		//		const double t1
//		//		double y[]
//		s = gsl_odeiv2_driver_apply(d, &x, x+h, y);
//
//		if (s != GSL_SUCCESS)
//		{
//			printf ("error: driver returned %d\n", s);
//			break;
//		}
//
//		// saving values to input ppointers
//		x_asdf[i] = x;
//		y_asdf[i] = y[0];
//	}
//
//	// free memory
//	gsl_odeiv2_driver_free(d);
//}


////
//// function calculates y(x), x in [-1,3] using GSL's implicit Euler
//// dy/dx = -2xx; y(-1) = 1/e
////
//void eq1_meth2(double *x_asdf, double *y_asdf, int n)
//{
//	double h = 4.0/n;
//
//	// fuction that will be input for gsl_odeiv2_system
//	int func(double x, const double y[], double f[], void *params)
//	{
//		f[0] = -2*x*y[0];
//		return GSL_SUCCESS;
//	}
//
//	// jacobian that will be input for gsl_odeiv2_system
//	int jac(double x, const double y[], double *dfdy, double dfdt[], void *params)
//	{
//		dfdy[0] = -2*x;
//		dfdt[0] = -2*y[0];
//		return GSL_SUCCESS;
//	}
//
//	// struct gsl_odeiv2_system has 4 inputs:
//	//		int (* function) (double t, const double y[], double dydt[], void * params)
//	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
//	//		size_t dimension
//	//		void * params
//	gsl_odeiv2_system sys = {func, jac, 1, 0};
//
//	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rk1imp;
//
//	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
//	//		const gsl_odeiv2_system * sys
//	//		const gsl_odeiv2_step_type * 
//	//		const double hstart
//	//		const double epsabs
//	//		const double epsrel
//	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&sys, type, h, INFINITY, INFINITY);
//
//	// this is xL...stepping below ensures we stop at xR
//	double x = -1.0;	
//
//	// initial (boundary) condition
//	double y[1] = { 1/M_E };
//
//	// function gsl_odeiv2_step_alloc has 2 inputs:
//	//		const gsl_odeiv2_step_type * T
//	//		size_t dim
//	gsl_odeiv2_step *step = gsl_odeiv2_step_alloc(type,1);
//
//	// function gsl_odeiv2_step_set_driver has 2 inputs:
//	//		gsl_odeiv2_step * s
//	//		const gsl_odeiv2_driver * d
//	gsl_odeiv2_step_set_driver (step, d);
//
//	double yerr[1];
//	double dydt_in[1];
//	double dydt_out[1];
//
//	GSL_ODEIV_FN_EVAL(&sys, x, y, dydt_in);
//
//	// stepping through and saving solution at every time step
//	int i = 0;
//	int s;
//	while (x<=3.0)
//	{
//		// function gsl_odeiv2_driver_apply has 4 inputs:
//		//		gsl_odeiv2_driver * d
//		//		double * t
//		//		const double t1
//		//		double y[]
//		s = gsl_odeiv2_step_apply(step, x, h, y, yerr, dydt_in, dydt_out, &sys);
//
//		if (s != GSL_SUCCESS)
//		{
//			printf ("error: driver returned %d\n", s);
//			break;
//		}
//
//		// saving values to input ppointers
//		x_asdf[i] = x;
//		y_asdf[i] = y[0];
//
//		// increment x
//		x = x+h;
//		i++;
//	}
//
//	// free memory
//	gsl_odeiv2_step_free(step);
//	gsl_odeiv2_driver_free(d);
//}


//
// solves eq2
//
void eq1_solve(double *x, double *y, int n, int sol_method)
{
	grvy_timer_begin(__func__);

	if(sol_method == 1)
	{
		eq1_meth1(x,y,n);
	}
	else
	{
		eq1_meth2(x,y,n);
	}

	grvy_timer_end(__func__);
}


//
// function calculates analytical solution and error of eq1
// requires that numerical solution has already been calculated
//
void eq1_verify(double *x, double *y, double *y_exact, double *e,
		double *e_Linf, int n)
{
	grvy_timer_begin(__func__);

	int i;

	// caluculate exact solution and error of numerical solution at each x[i]
	for(i=0 ; i<n+1 ; i++)
	{
		y_exact[i] = exp(-x[i]*x[i]);
		e[i] = y_exact[i] - y[i];
	}

	// calculate Linfinity error
	maxabs(e_Linf,e,n);

	grvy_timer_end(__func__);
}


//
// performs convergence analysis on eq1
//
void eq1_conv(double **x, double **y, double **y_exact, double **e, int **m,
		double **e_Linf, int a, int b, int sol_method, int *n)
{
	grvy_timer_begin(__func__);

	*n = pow(2,b);

	*m = malloc(((b-a)+1)*sizeof(int));
	test_malloc(*m);

	*e_Linf = malloc(((b-a)+1)*sizeof(double));
	test_malloc(*e_Linf);

	*x = malloc((*n+1)*sizeof(double));
	test_malloc(*x);

	*y = malloc((*n+1)*sizeof(double));
	test_malloc(*y);

	*y_exact = malloc((*n+1)*sizeof(double));
	test_malloc(*y_exact);

	*e = malloc((*n+1)*sizeof(double));
	test_malloc(*e);

	int k,j = 0;
	for(k=pow(2,a) ; k<*n+1 ; k=k*2)
	{
		(*m)[j] = k;
		eq1_solve(*x,*y,k,sol_method);
		eq1_verify(*x, *y, *y_exact, *e, &((*e_Linf)[j]), k);
		j++;
	}

	grvy_timer_end(__func__);
}

//
// function calculates .......... using GSL's rk4
//
void eq2_meth1(double *t_asdf, double *y_asdf, int n)
{
	grvy_timer_begin(__func__);

	double h = 25.0/n;

	// setting up a structure so that void *p can be cast to doubles
	struct func_params {double omega; double tau;};

	// function that will be used for gsl_odeiv2_system_sys
	int func(double t, const double y[], double f[], void *p)
	{
		struct func_params *params = (struct func_params *)p;
		double omega = (params->omega);
		double tau = (params->tau);
		f[0] = y[3];
		f[1] = y[4];
		f[2] = y[5];
		f[3] = omega*y[4] - y[3]/tau;
		f[4] = -omega*y[3] - y[4]/tau;
		f[5] = -y[5]/tau;
		return GSL_SUCCESS;
	}

	// parameters omega and tau
	double p[] = {5, 5};

	// struct gsl_odeiv2_system has 4 inputs:
	//		int (* function) (double t, const double y[], double dydt[], void * params)
	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
	//		size_t dimension
	//		void * params
	gsl_odeiv2_system sys = { func, 0, 6, &p};

	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rk4;

	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
	//		const gsl_odeiv2_system * sys
	//		const gsl_odeiv2_step_type * 
	//		const double hstart - seems to have no effect when using
	//		                      gsl_odeiv2_driver_apply_fixed_step (below)
	//		const double epsabs
	//		const double epsrel
	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new
		(&sys, type, h, INFINITY, INFINITY);

	// initial time
	double t = 0.0;

	// initial conditions (position and velocity
	double y[6] = { 0,0,0,20,0,2 };

	// saving values to input pointers
	t_asdf[0] = t;
	y_asdf[0] = y[0];
	y_asdf[1] = y[1];
	y_asdf[2] = y[2];

	// stepping through and saving solution at every time step
	int i, s;
	for(i=1 ; i<=n ; i++)
	{
		// function gsl_odeiv2_driver_apply_fixed_step has 5 inputs:
		//		gsl_odeiv2_driver * d
		//		double * t
		//		const double h
		//		const unsigned long int n
		//		double y[]
		s = gsl_odeiv2_driver_apply_fixed_step(d, &t, h, 1, y);

		if(s != GSL_SUCCESS)
		{
			printf("error: driver returned %d\n",s);
			break;
		}

		// saving values to input pointers
		t_asdf[i] = t;
		y_asdf[i*3] = y[0];
		y_asdf[i*3 + 1] = y[1];
		y_asdf[i*3 + 2] = y[2];
	}	

	// free memory
	gsl_odeiv2_driver_free(d);

	grvy_timer_end(__func__);
}


//
// function calculates .......... using GSL's rkf45
//
void eq2_meth2(double *t_asdf, double *y_asdf, int n)
{
	grvy_timer_begin(__func__);

	double h = 25.0/n;

	// setting up a structure so that void *p can be cast to doubles
	struct func_params {double omega; double tau;};

	// function that will be used for gsl_odeiv2_system_sys
	int func(double t, const double y[], double f[], void *p)
	{
		struct func_params *params = (struct func_params *)p;
		double omega = (params->omega);
		double tau = (params->tau);
		f[0] = y[3];
		f[1] = y[4];
		f[2] = y[5];
		f[3] = omega*y[4] - y[3]/tau;
		f[4] = -omega*y[3] - y[4]/tau;
		f[5] = -y[5]/tau;
		return GSL_SUCCESS;
	}

	// parameters omega and tau
	double p[] = {5, 5};

	// struct gsl_odeiv2_system has 4 inputs:
	//		int (* function) (double t, const double y[], double dydt[], void * params)
	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
	//		size_t dimension
	//		void * params
	gsl_odeiv2_system sys = { func, 0, 6, &p};

	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rkf45;

	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
	//		const gsl_odeiv2_system * sys
	//		const gsl_odeiv2_step_type * 
	//		const double hstart - seems to have no effect when using
	//		                      gsl_odeiv2_driver_apply_fixed_step (below)
	//		const double epsabs
	//		const double epsrel
	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new
		(&sys, type, h, INFINITY, INFINITY);

	// initial time
	double t = 0.0;

	// initial conditions (position and velocity
	double y[6] = { 0,0,0,20,0,2 };

	// saving values to input pointers
	t_asdf[0] = t;
	y_asdf[0] = y[0];
	y_asdf[1] = y[1];
	y_asdf[2] = y[2];

	// stepping through and saving solution at every time step
	int i, s;
	for(i=1 ; i<=n ; i++)
	{
		// function gsl_odeiv2_driver_apply_fixed_step has 5 inputs:
		//		gsl_odeiv2_driver * d
		//		double * t
		//		const double h
		//		const unsigned long int n
		//		double y[]
		s = gsl_odeiv2_driver_apply_fixed_step(d, &t, h, 1, y);

		if(s != GSL_SUCCESS)
		{
			printf("error: driver returned %d\n",s);
			break;
		}

		// saving values to input pointers
		t_asdf[i] = t;
		y_asdf[i*3] = y[0];
		y_asdf[i*3 + 1] = y[1];
		y_asdf[i*3 + 2] = y[2];
	}	

	// free memory
	gsl_odeiv2_driver_free(d);

	grvy_timer_end(__func__);
}


//
// function calculates .......... using GSL's rk8pd
//
void eq2_meth3(double *t_asdf, double *y_asdf, int n)
{
	grvy_timer_begin(__func__);

	double h = 25.0/n;

	// setting up a structure so that void *p can be cast to doubles
	struct func_params {double omega; double tau;};

	// function that will be used for gsl_odeiv2_system_sys
	int func(double t, const double y[], double f[], void *p)
	{
		struct func_params *params = (struct func_params *)p;
		double omega = (params->omega);
		double tau = (params->tau);
		f[0] = y[3];
		f[1] = y[4];
		f[2] = y[5];
		f[3] = omega*y[4] - y[3]/tau;
		f[4] = -omega*y[3] - y[4]/tau;
		f[5] = -y[5]/tau;
		return GSL_SUCCESS;
	}

	// parameters omega and tau
	double p[] = {5, 5};

	// struct gsl_odeiv2_system has 4 inputs:
	//		int (* function) (double t, const double y[], double dydt[], void * params)
	//		int (* jacobian) (double t, const double y[], double * dfdy, double dfdt[], void * params)
	//		size_t dimension
	//		void * params
	gsl_odeiv2_system sys = { func, 0, 6, &p};

	const gsl_odeiv2_step_type *type = gsl_odeiv2_step_rk8pd;

	// function gsl_odeiv2_driver_alloc_y_new has 5 inputs:
	//		const gsl_odeiv2_system * sys
	//		const gsl_odeiv2_step_type * 
	//		const double hstart - seems to have no effect when using
	//		                      gsl_odeiv2_driver_apply_fixed_step (below)
	//		const double epsabs
	//		const double epsrel
	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new
		(&sys, type, h, INFINITY, INFINITY);

	// initial time
	double t = 0.0;

	// initial conditions (position and velocity
	double y[6] = { 0,0,0,20,0,2 };

	// saving values to input pointers
	t_asdf[0] = t;
	y_asdf[0] = y[0];
	y_asdf[1] = y[1];
	y_asdf[2] = y[2];

	// stepping through and saving solution at every time step
	int i, s;
	for(i=1 ; i<=n ; i++)
	{
		// function gsl_odeiv2_driver_apply_fixed_step has 5 inputs:
		//		gsl_odeiv2_driver * d
		//		double * t
		//		const double h
		//		const unsigned long int n
		//		double y[]
		s = gsl_odeiv2_driver_apply_fixed_step(d, &t, h, 1, y);

		if(s != GSL_SUCCESS)
		{
			printf("error: driver returned %d\n",s);
			break;
		}

		// saving values to input pointers
		t_asdf[i] = t;
		y_asdf[i*3] = y[0];
		y_asdf[i*3 + 1] = y[1];
		y_asdf[i*3 + 2] = y[2];
	}	

	// free memory
	gsl_odeiv2_driver_free(d);

	grvy_timer_end(__func__);
}


//
// solves eq2
//
void eq2_solve(double *t, double *y, int n, int sol_method)
{
	grvy_timer_begin(__func__);

	if(sol_method == 1)
	{
		eq2_meth1(t,y,n);
	}
	else if(sol_method == 2)
	{
		eq2_meth2(t,y,n);
	}
	else
	{
		eq2_meth3(t,y,n);
	}

	grvy_timer_end(__func__);
}


//
// performs convergence analysis on eq2
//
void eq2_conv(double **t, double **y, double **y_exact, int **m,
		double **e_Linf, int a, int b, int sol_method, int *n)
{
	grvy_timer_begin(__func__);

	*n = pow(2,b);

	*m = malloc((b-a)*sizeof(int));;
	test_malloc(*m);

	*e_Linf = malloc((b-a)*sizeof(double));
	test_malloc(*e_Linf);

	*t = malloc(((*n)+1)*sizeof(double));
	test_malloc(*t);

	*y = malloc(3*((*n)/2+1)*sizeof(double));
	test_malloc(*y);

	*y_exact = malloc(3*((*n)+1)*sizeof(double));
	test_malloc(*y_exact);

	// solve at finest discretization
	// we will consider this to be the "exact" solution
	// for convergence analysis
	eq2_solve(*t,*y_exact,*n,sol_method);

	// calculate at coarser discretizations and compare
	int i, k, j=0;
	double t2[*n/2+1];
	for(k=pow(2,a); k<*n ; k=k*2)
	{
		(*m)[j] = k;
		eq2_solve(t2,*y,k,sol_method);
		int rat = (*n)/k;
		double e;	// L2 error (in 3D) at a particular time step
		(*e_Linf)[j] = -INFINITY;
		for(i=0 ; i<k+1 ; i++)
		{
			e = sqrt(pow((*y)[3*i] - (*y_exact)[rat*3*i],2)
					+ pow((*y)[3*i+1] - (*y_exact)[rat*3*i+1],2)
					+ pow((*y)[3*i+2] - (*y_exact)[rat*3*i+2],2));
			if(e > (*e_Linf)[j])
			{
				(*e_Linf)[j] = e;
			}
		}
		j++;
	}

	grvy_timer_end(__func__);
}


//
// function maxabs
//
void maxabs(double *max, double *e, int n)
{
	*max = -INFINITY;
	int i = 0;
	double test;
	while(i<n+1)
	{
		test = fabs(e[i]);
		if(test > *max)
		{
			*max = test;
		}
		i++;
	}
}


//
// allocates and tests if successful
//
//void my_malloc(void **ptr, size_t a)
//{
//	*ptr = malloc(a);
//	if(*ptr == NULL)
//	{
//		printf("Memory allocation error\n");
//		exit(77);
//	}
//}


//
// tests whether malloc was successful
//
void test_malloc(void *ptr)
{
	if (ptr == NULL)
	{
		printf("Memory allocation error\n");
		exit(77);
	}
}

//
// write data to file(s)
//
void write_output(double *x, double *y, double *y_exact, double *e,
		int *m, double *e_Linf, int eq_num, int sol_method, int conv,
		int ver_mode, int n, int a, int b, int verb)
{
	grvy_timer_begin(__func__);

	int i;
	char filename[50];

	int zz = 0;
	if(verb == 0)
		zz = (n-1)/10000;	// limiting output
	if(zz == 0)
		zz = 1;

	if(conv == 1)		// convergence analysis results
	{
		int lim;

		if(eq_num == 1)
			lim = b-a+1;
		else
			lim = b-a;

		sprintf(filename,"results_eq%d_sol%d_convergence.dat",
				eq_num,sol_method);
		FILE *ofp_convergence = fopen(filename,"w");

		fprintf(ofp_convergence,"n\tLinf error\n");

		for(i=0 ; i<lim ; i++)
			fprintf(ofp_convergence,"%d\t%E\n",m[i],e_Linf[i]);

		fclose(ofp_convergence);
	}

	FILE *ofp_results;

	if(eq_num == 1)			// equation 1 results
	{

		sprintf(filename,"results_eq%d_sol%d_ver%d_verb%d_%d.dat",
				eq_num,sol_method,ver_mode,verb,n);
		ofp_results = fopen(filename,"w");

		if(ver_mode == 1)	// verification mode
		{
			if(conv == 1)
				fprintf(ofp_results,"Linfinity error = %E\n\n",e_Linf[b-a]);
			else
				fprintf(ofp_results,"Linfinity error = %E\n\n",*e_Linf);

			fprintf(ofp_results,
					"x\t\t\t\ty_num\t\t\ty_exact\t\t\terror\n");

			for(i=0 ; i<n ; i=i+zz)
				fprintf(ofp_results,"%E\t%E\t%E\t%E\t\n",
						x[i],y[i],y_exact[i],e[i]);

			fprintf(ofp_results,"%E\t%E\t%E\t%E\t\n",
					x[n],y[n],y_exact[n],e[n]);
		}
		else				// no verification mode
		{
			fprintf(ofp_results,"x\t\t\t\ty\n");

			for(i=0 ; i<n ; i=i+zz)
				fprintf(ofp_results,"%E\t%E\n",	x[i],y[i]);

			fprintf(ofp_results,"%E\t%E\n",	x[n],y[n]);
		}
	}
	else					// equation 2 results
	{

		sprintf(filename,"results_eq%d_sol%d_verb%d_%d.dat",
				eq_num,sol_method,verb,n);
		ofp_results = fopen(filename,"w");

		fprintf(ofp_results,"t\t\t\t\tx\t\t\t\ty\t\t\t\tz\n");

		if(conv == 1)
		{
			for(i=0 ; i<n ; i=i+zz)
				fprintf(ofp_results,"%E\t%E\t%E\t%E\n",
						x[i],y_exact[3*i],y_exact[3*i+1],y_exact[3*i+2]);

			fprintf(ofp_results,"%E\t%E\t%E\t%E\n",
					x[n],y_exact[3*n],y_exact[3*n+1],y_exact[3*n+2]);
		}
		else
		{
			for(i=0 ; i<n+1 ; i=i+zz)
				fprintf(ofp_results,"%E\t%E\t%E\t%E\n",
						x[i],y[3*i],y[3*i+1],y[3*i+2]);

			fprintf(ofp_results,"%E\t%E\t%E\t%E\n",
					x[n],y[3*n],y[3*n+1],y[3*n+2]);
		}
	}
	fclose(ofp_results);

	grvy_timer_end(__func__);
}
