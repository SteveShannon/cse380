#!/bin/bash

outfile=`echo $1 | cut -d . -f 1`.png
sol_meth_num=`echo $1 | cut -c 16`


if [[ $1 = *"eq1"* ]]; then
	if [[ $sol_meth_num -eq 1 ]]; then
		sol_meth="self-implemented implicit Euler"
	else
		sol_meth="GSL implicit Euler - rk1imp"
	fi
	if [[ $1 = *"convergence"* ]]; then
		gnuplot << HERE
		set terminal png
		set output '$outfile'
		set logscale xy
		set title "convergence results of numerical integration of equation 1:\\ndy/dx=-2xy; y(-1)=1/e\\nusing soltion method $sol_meth_num: $sol_meth"
		set xlabel "number of steps, n"
		set ylabel "L infinity error"
		plot '$1' using 1:2 w linespoints notitle,\
			1/x title "log-log slope: -1"
HERE
	elif [[ $1 = *"ver0"* ]]; then
		gnuplot << HERE
		set terminal png
		set output '$outfile'
		set title "results of numerical integration of equation 1:\\ndy/dx=-2xy; y(-1)=1/e\\nusing solution method $sol_meth_num: $sol_meth"
		set xlabel "x"
		set ylabel "y"
		plot '$1' using 1:2 w lines notitle"
HERE
	elif [[ $1 = *"ver1"* ]]; then
		gnuplot << HERE
		set terminal png size 640,960
		set output '$outfile'
		set multiplot layout 2,1
		set title "results of numerical integration of equation 1:\\ndy/dx=-2xy; y(-1)=1/e\\nusing solution method $sol_meth_num: $sol_meth"
		set xlabel "x"
		set ylabel "y"
		plot '$1' using 1:2 w lines notitle"
		set title "error of numerical integration of equation 1:\\ndy/dx=-2xy; y(-1)=1/e\\nusing solution method $sol_meth_num: $sol_meth"
		set xlabel "x"
		set ylabel "e"
		plot '$1' using 1:4 w lines notitle"
		unset multiplot
HERE
	fi
elif [[ $1 = *"eq2"* ]]; then
	if [[ $sol_meth_num -eq 1 ]]; then
		sol_meth="explicit 4th order RK (GSL rk4)"
		slope=4
	elif [[ $sol_meth_num -eq 2 ]]; then
		sol_meth="explicit 4-5 RKF (GSL rkf45)"
		slope=5
	else
		sol_meth="explicit 8-9 RKPD (GSL rk8pd)"
		slope=9
	fi
	if [[ $1 = *"convergence"* ]]; then
		gnuplot << HERE
		set terminal png
		set output '$outfile'
		set logscale xy
		set title "convergence results of numerical integration of equation 2:\\ntrajectory of a charged particle in a vertical electric field\\nusing solution method $sol_meth_num: $sol_meth"
		set xlabel "number of steps, n"
		set ylabel "L infinity error (of Euclidean distances at time steps)"
		plot '$1' using 1:2 w linespoints notitle,\
			1000000/x**$slope title "log-log slope: -$slope"
HERE
	else
		gnuplot << HERE
		set terminal png size 640,640
		set output '$outfile'
		set ticslevel 0
		set title "results of numerical integration of equation 2:\\ntrajectory of a charged particle in a vertical electric field\\nusing solution method $sol_meth_num: $sol_meth"
		set xlabel "x"
		set ylabel "y"
		set zlabel "z"
		splot '$1' using 2:3:4 w lines notitle
HERE
	fi
fi
