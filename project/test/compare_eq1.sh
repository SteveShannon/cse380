#!/bin/bash

#
# running test case and comparing against reference solution
#

if [ $# -ne 2 ]; then
	exit 2
fi

ref_sol=$1
computed_sol=$2

# setting tolerance of 1E-08
tol=`echo 0.00000001 | bc -l`

# reading in values from reference solution
j=0
while read line; do
	j=$(($j+1))
	y1[$j]=`echo $line |cut -d " " -f 2`
done < $ref_sol 

# reading in values from solution computed from test
j=0
while read line; do
	j=$(($j+1))
	y2[$j]=`echo $line |cut -d " " -f 2`
done < $computed_sol

# looping through all reference solution - computed solution pairs
for i in `seq 2 $j`; do

	# taking the difference between solution values
	diffy=`echo "${y1[$i]}-(${y2[$i]})" | sed -r 's/E\+*/*10^/g' | bc -l`

	# taking the absolute value of the difference
	diffy=`echo $diffy | sed 's/-//g'`

	# comparing to difference to tolerance
	faily=`echo "$diffy > $tol" | bc -l`

	if [[ $faily == 1 ]]; then
		exit 1 
	fi

done
