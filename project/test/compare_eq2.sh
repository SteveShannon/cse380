#!/bin/bash

#
# running test case and comparing against reference solution
#

if [ $# -ne 2 ]; then
	echo "something is fooked"
	exit 36
fi

ref_sol=$1
computed_sol=$2

# setting tolerance of 1E-08
tol=`echo 0.00000001 | bc -l`

# reading in values from reference solution
j=0
while read line; do
	j=$(($j+1))
	x1[$j]=`echo $line |cut -d " " -f 2`
	y1[$j]=`echo $line |cut -d " " -f 3`
	z1[$j]=`echo $line |cut -d " " -f 4`
done < $ref_sol 

# reading in values from solution computed from test
j=0
while read line; do
	j=$(($j+1))
	x2[$j]=`echo $line |cut -d " " -f 2`
	y2[$j]=`echo $line |cut -d " " -f 3`
	z2[$j]=`echo $line |cut -d " " -f 4`
done < $computed_sol

# looping through all reference solution - computed solution pairs
# (coordinate by coordinate...checking that computed solution lies within
# epsilon cube)
for i in `seq 2 $j`; do
	diffx=`echo "${x1[$i]}-(${x2[$i]})" | sed -r 's/E\+*/*10^/g' | bc -l`
	diffx=`echo $diffx | sed 's/-//g'`
	failx=`echo "$diffx > $tol" | bc -l`

	diffy=`echo "${y1[$i]}-(${y2[$i]})" | sed -r 's/E\+*/*10^/g' | bc -l`
	diffy=`echo $diffy | sed 's/-//g'`
	faily=`echo "$diffy > $tol" | bc -l`

	diffz=`echo "${z1[$i]}-(${z2[$i]})" | sed -r 's/E\+*/*10^/g' | bc -l`
	diffz=`echo $diffz | sed 's/-//g'`
	failz=`echo "$diffz > $tol" | bc -l`

	if [[ $failx == 1 || $faily == 1 || $failz == 1 ]]; then
		exit 1 
	fi

done
