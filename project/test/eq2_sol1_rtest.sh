#!/bin/bash

EXE="../src/main"

SOLFILE="./results_eq2_sol1_verb1_64.dat"
SOLREF="./test_output_eq2_sol1.dat"
INFILE="./test_input_eq2_sol1.dat"

rm -f $SOLFILE

./$EXE $INFILE

./compare_eq2.sh $SOLREF $SOLFILE

#exit $?
