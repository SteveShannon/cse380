#!/bin/bash

EXE="../src/main"

SOLFILE="./results_eq1_sol1_ver0_verb1_64.dat"
SOLREF="./test_output_eq1_sol1.dat"
INFILE="./test_input_eq1_sol1.dat"

rm -f $SOLFILE

./$EXE $INFILE

./compare_eq1.sh $SOLREF $SOLFILE

#exit $?
