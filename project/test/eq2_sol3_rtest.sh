#!/bin/bash

EXE="../src/main"

SOLFILE="./results_eq2_sol3_verb1_64.dat"
SOLREF="./test_output_eq2_sol3.dat"
INFILE="./test_input_eq2_sol3.dat"

rm -f $SOLFILE

./$EXE $INFILE

./compare_eq2.sh $SOLREF $SOLFILE

#exit $?
