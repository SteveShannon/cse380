#include <stdio.h>
#include <math.h>		/* I need this for log and fabs functions */

int main(){
	int i;
	int num_pts = 1000;
	double ln_fact[num_pts];
	double stirling[num_pts];
	double abs_err[num_pts];
	double rel_err[num_pts];

	/* Calculating natural log of factorials (ln(N!)=ln1+ln2+...) */
	ln_fact[0] = log(1);
	for(i = 1 ; i < num_pts ; i = i+1){
		ln_fact[i] = ln_fact[i-1] + log(i+1);
	}

//	/* Alternate method: computing factorials and then taking log */
//	ln_fact[0]=1;
//	for(i = 1 ; i < num_pts ; i = i+1){
//		ln_fact[i] = (i+1) * ln_fact[i-1];		/* this becomes inf at i=172 (171!) */	
//	}
//	for(i = 0 ; i <= num_pts - 1 ; i = i+1){
//		ln_fact[i] = log(ln_fact[i]);
//	}

	/* Using stirling's formula, finding absolute and relative error */
	for(i = 0 ; i < num_pts ; i = i+1){
		stirling[i] = (i+1) * log(i+1) - (i+1);
		abs_err[i] = fabs (ln_fact[i] - stirling[i]);
		rel_err[i] = abs_err[i]/ln_fact[i];
	}

	/* printing results to a file */
	FILE *fp;
	fp=fopen("./results.dat", "w");		/* "w" means write */
	for(i = 0 ; i < num_pts ; i = i+1){
		fprintf(fp,"%d %.1lf %lf %lf %lf\n",i+1,ln_fact[i],stirling[i],abs_err[i],rel_err[i]);	 /* try %.3lf, for example */
	}

	/* Plot results */
	int num_commands = 10;
	char *commandsForGnuplot[] = {"set terminal png size 600,750",
								"set output 'results.png'",
								"set multiplot layout 2,1",
								"set xlabel \"N\"",
								"set ylabel \"absolute error of Stirling's formula\"",
								"plot 'results.dat' using 1:4 w lines notitle",
								"set xlabel\"N\"",
								"set ylabel \"relative error of Stirling's formula\"",
								"set logscale xy",
								"plot 'results.dat' using 1:5 w lines notitle"};
	FILE *gnuplotPipe;
	gnuplotPipe = popen("gnuplot", "w");
	for(i=0 ; i < num_commands ; i = i+1){
		fprintf(gnuplotPipe,"%s\n",commandsForGnuplot[i]);
	}

	fclose(fp);
	fclose(gnuplotPipe);	

	return 0;
}
