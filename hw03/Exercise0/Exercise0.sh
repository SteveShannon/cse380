#/bin/bash

# compile the code
gcc -o Exercise0 Exercise0.c -lm	# we need -lm to link to math library

# run executable
./Exercise0

# open plot of results
gnome-open results.png

