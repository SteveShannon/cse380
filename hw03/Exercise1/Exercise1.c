#include <stdio.h>
#include <math.h>
#include <gsl/gsl_multifit.h>

int main(){
	int i,j,n,m;
	n=10;	// number of data points
	m=7;	// solving for 7 coefficients


	// reading input from a file and populating r_ and E_ arrays
	double temp_a, temp_b, r_[n], E_[n];
	FILE *ifp = fopen("./input.dat", "r");
	char line[100];
	int hits, k=0;
	while(fgets(line,sizeof(line),ifp) != NULL){	// this will, line by line, read input.dat and assign it to line, and exit at end of file
		hits=sscanf(line,"%lf %lf",&temp_a,&temp_b);	// hits is number of successful reads (checking to see if we are reading 2 doubles or not)
		if(hits==2){
			r_[k]=temp_a;
			E_[k]=temp_b;
			k++;
		}
	}

//	// Alternate method...this only works if input.dat has no column headers (must have 2 columns of doubles and nothing else)
//	// fscanf returns number of inputs: 0 if not in correct format (and does not advance to the next line!!!)
//	//                                 -1 if at the end of the file
//
//	double a[10], b[10];
//	FILE *ifp = fopen("./input_backup.dat", "r");
//
//	int number;
//	for(i=0 ; i < 10 ; i++){
//		number=fscanf(ifp,"%lf %lf",&a[i],&b[i]);		// %f does not work here
//		printf("number %d ",number);
//		printf("a %lf, b %lf\n",a[i],b[i]);
//	}


	double c_poly_[m],c_exp_[m];
	
	// variables for use in gsl_multifit_linear
	gsl_vector *E,*r,*c_poly,*c_exp;	// E: energy (input), r: Angstroms (input), c: coefficients (output)
	gsl_matrix *Z_poly,*Z_exp,*cov;		// Z: matrix of polynomials or exponentials (input), cov: covariance (output)
	double chisq;						// chi squared (output)
	c_poly=gsl_vector_alloc(m);
	c_exp=gsl_vector_alloc(m);
	E=gsl_vector_alloc(n);
	Z_poly=gsl_matrix_alloc(n,m);
	Z_exp=gsl_matrix_alloc(n,m);
	cov=gsl_matrix_alloc(m,m);

	// assigning values to E vector and Z matrix (system Z*c=E)
	for(i=0 ; i<n ; i++){
		gsl_vector_set(E,i,E_[i]);
		for(j=0; j<m ; j++){
			gsl_matrix_set(Z_poly,i,j,pow(r_[i],j));
			gsl_matrix_set(Z_exp,i,j,exp(-j*r_[i]));
		}
	}
	
	gsl_multifit_linear_workspace *work;
	work=gsl_multifit_linear_alloc(n,m);	

	gsl_multifit_linear(Z_poly,E,c_poly,cov,&chisq,work);
	gsl_multifit_linear(Z_exp,E,c_exp,cov,&chisq,work);

	gsl_multifit_linear_free(work);

	#define c_poly(j) (gsl_vector_get(c_poly,j))	
	#define c_exp(j) (gsl_vector_get(c_exp,j))	
	for(j = 0 ; j < m ; j++){
		c_poly_[j]=c_poly(j);
		c_exp_[j]=c_exp(j);
	}
	
	// creating best fit lines from the calculated coefficients
	double xL=0.2;
	double xR=5.0;
	double h=0.05;
	int p=(xR-xL)/h+1;
	double x[p], y_poly[p], y_exp[p];
	FILE *fp = fopen("./output.dat", "w");
	for(i=0 ; i<p ; i++){
		x[i] = xL + i*h;
		y_poly[i] = 0;
		y_exp[i] = 0;
		for(j=0 ; j<m ; j++){
			y_poly[i] = y_poly[i] + c_poly_[j]*pow(x[i],j);
			y_exp[i] = y_exp[i] + c_exp_[j]*exp(-j*x[i]);
		}
   	    fprintf(fp,"%lf %lf %lf\n",x[i],y_poly[i],y_exp[i]);     // printing to a file...try %.8lf, for example
	}


    // Plot results
    int num_commands = 13;
    char *commandsForGnuplot[] = {"set terminal png size 600,750",
                                "set output 'results.png'",
								"set multiplot layout 2,1",
								"set title \"fit to 6th order polynomial in terms of r^p\"",
                                "set xlabel \"r (Angstroms)\"",
                                "set ylabel \"Energy (hartrees)\"",
                                "plot 'output.dat' using 1:2 w lines notitle,\\",
								"'input.dat' using 1:2 w points notitle",
								"set title \"fit to 6th order polynomial in terms of exp(-pr)\"",
								"set xlabel \"r (Angstroms)\"",
								"set ylabel \"Energy (hartrees)\"",
								"plot 'output.dat' using 1:3 w lines notitle,\\",
								"'input.dat' using 1:2 w points notitle"};
    FILE *gnuplotPipe = popen("gnuplot", "w");
    for(i=0 ; i < num_commands ; i++){
        fprintf(gnuplotPipe,"%s\n",commandsForGnuplot[i]);
    }

	fclose(ifp);
    fclose(fp);
    fclose(gnuplotPipe);

	return 0;
}
