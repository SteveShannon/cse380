#!/bin/bash

# Note: verify gsl module is loaded first as well as gnu

# compile and link - stampede
#gcc -o Exercise1 -I$TACC_GSL_INC Exercise1.c -lm -lgsl -lgslcblas -L$TACC_GSL_LIB

# compile and link - germain
gcc -o Exercise1 -I$GSL_INC Exercise1.c -lm -lgsl -lgslcblas -L$GSL_LIB

# compile and link - my laptop
#gcc -o Exercise1 Exercise1.c -lm -lgsl -lgslcblas 

# run the executable
./Exercise1

# open the plot
display results.png
