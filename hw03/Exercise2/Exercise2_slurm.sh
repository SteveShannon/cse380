#!/bin/bash
#SBATCH -p development
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -o stuff.%j.out
#SBATCH -e error.%j.out
#SBATCH -J hw03-Exercise2
#SBATCH -P normal
#SBATCH -t 00:03:00
#SBATCH --dependency=
#SBATCH --mail-user=stephenshannon1120@gmail.com
# #SBATCH --mail-type=all

module load gsl
ibrun ./Exercise2
