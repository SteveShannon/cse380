#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>


const double erfx_exact = 0.8427007929497148693412206350826092592960669979663029084599;	// for x=1 (change this for different x...source: Wolfram Alpha)
const double x = 1.0;


// function for performing integration using trapezoidal rule with different numbers of "elements"
void trap(){
	int	num_eval;
	double h, temp;
	int i, j, N, Np;
	num_eval = 20;
	FILE *ofp_trap = fopen("./results_trap.dat","w");
	Np = 2;
	N = Np-1;
	h = x/(Np-1);
	double erfx_trap_sum=0, erfx_trap, e_trap;
	int size = (int)(pow(2,num_eval-1)+1);	// this is called casting
	double *y = malloc(size*sizeof(double));	
//	double y[(int)(pow(2,num_eval-1)+1)];	// defining in this way uses stack...the stack can not handle huge arrays; defining with malloc uses heap
	y[0] = 0.0;
	y[Np-1] = x;
	for(i=1 ; i<Np-1 ; i++){
		y[i] = y[i-1] + h;
		erfx_trap_sum = exp(-pow(y[i],2));
	}
	erfx_trap_sum =  2*erfx_trap_sum + 1 + exp(-pow(x,2));
	erfx_trap = h/M_SQRTPI*erfx_trap_sum;
	e_trap = fabs(erfx_trap - erfx_exact);
	fprintf(ofp_trap,"%d %d %E %E\n",N,Np,erfx_trap,e_trap);

	for(j=1 ; j<num_eval ; j++){	// dividing h by 2 and re-using old data
		Np = 2*Np - 1;
		N = Np-1;
		h = x/(Np-1);
		y[Np-1] = x;

		for(i=1 ; i<Np-1 ; i++){
			y[i] = y[i-1] + h;
		}

		for(i=0 ; i<(Np-1)/2 ; i++){		// calculate only at odd indices
			erfx_trap_sum = erfx_trap_sum + 2*exp(-pow(y[i*2+1],2));
		}

		erfx_trap = h/M_SQRTPI*erfx_trap_sum;
		e_trap = fabs(erfx_trap - erfx_exact);
		fprintf(ofp_trap,"%d %d %E %E\n",N,Np,erfx_trap,e_trap);
	}
	free(y);
	fclose(ofp_trap);
}


// function for performing integration using Simpson's (1/3) rule with different numbers of "elements"
void simp(){
	int num_eval = 6;
	double h, temp;
	int i, j, N, Np;
	FILE *ofp_simp = fopen("./results_simp.dat","w");
	Np = 3;
	N = (Np-1)/2;
	h = x/(Np-1);
	double erfx_simp_sum=0, erfx_simp, e_simp;
	int size = (int)(pow(3,num_eval-1)*2+1);	// this is called casting
	double *y = malloc(size*sizeof(double));	
//	double y[(int)(pow(2,num_eval-1)+1)];	// defining in this way uses stack...the stack can not handle huge arrays; defining with malloc uses heap
	y[0] = 0.0;
	y[Np-1] = x;
	for(i=1 ; i<Np-1 ; i++){
		y[i] = y[i-1] + h;
	}

	erfx_simp_sum = erfx_simp_sum + 1 + 4*exp(-pow(x/2,2)) + exp(-pow(x,2));
	erfx_simp = 2*h/(3*M_SQRTPI)*erfx_simp_sum;
	e_simp = fabs(erfx_simp - erfx_exact);
	fprintf(ofp_simp,"%d %d %lf %E\n",N,Np,erfx_simp,e_simp);

	for(j=1 ; j<num_eval ; j++){
		Np = 3*Np - 2;
		N = (Np-1)/2;
		h = x/(Np-1);
		y[Np-1] = x;		
	
		for(i=1 ; i<Np-1 ; i++){
			y[i] = y[i-1] + h;
		}

		for(i=0 ; i<(Np-1)/6 ; i++){
			erfx_simp_sum = erfx_simp_sum + 4*exp(-pow(y[6*i+1],2));
			erfx_simp_sum = erfx_simp_sum + 4*exp(-pow(y[6*i+5],2));
			erfx_simp_sum = erfx_simp_sum + 2*exp(-pow(y[6*i+2],2));
			erfx_simp_sum = erfx_simp_sum + 2*exp(-pow(y[6*i+4],2));
		}
		erfx_simp = 2*h/(3*M_SQRTPI)*erfx_simp_sum;
		e_simp = fabs(erfx_simp - erfx_exact);
		fprintf(ofp_simp,"%d %d %lf %E\n",N,Np,erfx_simp,e_simp);
	}

	fclose(ofp_simp);
	free(y);
}


// function to be used in gsl_monte_function, which in turn is used in gsl_monte_plain_integrate
double f (double *x, size_t dim, void *params){
	return 2/sqrt(M_PI)*exp(-pow(*x,2));
}	

// function for performing Monte Carlo integration with different numbers of points
void mont(){
	int i;
	double res, err, e;
	double xl = 0;
	gsl_monte_function F = {&f,1,0};	// {double (*function)(double *x,size_t dim, void *params), size_t dim, void *params}
	size_t Np;
	gsl_rng_env_setup();
	const gsl_rng_type *T = gsl_rng_default;
	gsl_rng *r = gsl_rng_alloc(T);
	gsl_monte_plain_state *s = gsl_monte_plain_alloc(1);
	FILE *ofp_mont = fopen("./results_mont.dat","w");
	for(i=2; i<100000000 ; i=i*2){
		Np = (size_t)i;
		gsl_monte_plain_integrate(&F,&xl,&x,1,Np,r,s,&res,&err);
		e = fabs(erfx_exact-res);
		fprintf(ofp_mont,"%d %lf %E\n",i,res,e);
	}
	fclose(ofp_mont);
	gsl_monte_plain_free(s);
}


// function to be used in gsl_function, which in turn is used in gsl_integration_qag
double g (double x, void *params){
	return 2/sqrt(M_PI)*exp(-pow(x,2));
}

// function for performing Guass quadrature using gsl_integration_qag - only requires one "step" with 15 points to "converge"
void quad(){
	size_t n = 100;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(n);
	double result, error;
	double expected = erfx_exact;
	double xl = 0;
	double epsabs = 1E-10;		// desired absolute error limit
	double epsrel = 1E-10;		// desired relative error limit
	int key = 1;				// key = 1 corresponds to 15 point Gauss-Kronrod
	size_t limit = 100;
	gsl_function G = {&g,0};	// {double (*f)(double x, void *params), void *params}
	gsl_integration_qag(&G,xl,x,epsabs,epsrel,limit,key,w,&result,&error);
	gsl_integration_workspace_free(w);
	double e = fabs(erfx_exact-result);
	FILE *ofp_quad = fopen("./results_quad.dat","w");
	fprintf(ofp_quad,"15 %lf %E\n",result,e);
	fclose(ofp_quad);
}


// run the four integration schemes
int main(){
	trap();
	simp();
	mont();
	quad();
	return 0;
}
