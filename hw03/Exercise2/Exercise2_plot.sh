#!/bin/bash

# after running executable ./Exercise2, running this script creates a plot

gnuplot << HERE
set terminal png
set output 'results.png'
set logscale xy
set title "error in numerical integration of erf(1)"
set xlabel "number of evaluation points"
set ylabel "absolute error"
plot 'results_trap.dat' using 2:4 w linespoints title "Trapezoidal rule",\
'results_simp.dat' using 2:4 w linespoints title "Simpson's rule",\
'results_mont.dat' using 1:3 w linespoints title "Monte Carlo",\
'results_quad.dat' using 1:3 w linespoints title "Gauss quadrature"
HERE

gnuplot << HERE
set terminal png
set output 'results_trap.png'
set logscale xy
set title "convergence of trapezoidal rule"
set xlabel "number of evaluation points"
set ylabel "absolute error"
plot 'results_trap.dat' using 2:4 w linespoints title "Trapezoidal rule",\
1/x**2 title "log-log slope: -2"
HERE

gnuplot << HERE
set terminal png
set output 'results_simp.png'
set logscale xy
set title "convergence of Simpson's rule"
set xlabel "number of evaluation points"
set ylabel "absolute error"
plot 'results_simp.dat' using 2:4 w linespoints title "Simpson's rule",\
1/x**4 title "log-log slope: -4"
HERE

gnuplot << HERE
set terminal png
set output 'results_mont.png'
set logscale xy
set title "convergence of Monte Carlo integration"
set xlabel "number of evaluation points"
set ylabel "absolute error"
plot 'results_mont.dat' using 1:3 w linespoints title "Monte Carlo",\
1/sqrt(x) title "log-log slope: -1/2"
HERE
