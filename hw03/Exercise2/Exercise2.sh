#!/bin/bash

# Run this script to compile, run, plot, open plot all in one shot


# compile and link - stampede - make sure gsl module is loaded first
make

# compile and link - germain - make sure gsl module is loaded first (sl6 and gcc before that)
#gcc -o Exercise2 -I$GSL_INC Exercise2.c -lm -lgsl -lgslcblas -L$GSL_LIB

# compile and link - my laptop
#gcc -g -o Exercise2 Exercise2.c -lm -lgsl -lgslcblas

# run the executable
./Exercise2

# run the plotting script
./Exercise2_plot.sh

# open the plot
display results.png

